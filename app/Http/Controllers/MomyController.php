<?php

namespace App\Http\Controllers;

use App\Models\Momy;
use App\Http\Requests\StoreMomyRequest;
use App\Http\Requests\UpdateMomyRequest;

class MomyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMomyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMomyRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Momy  $momy
     * @return \Illuminate\Http\Response
     */
    public function show(Momy $momy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Momy  $momy
     * @return \Illuminate\Http\Response
     */
    public function edit(Momy $momy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMomyRequest  $request
     * @param  \App\Models\Momy  $momy
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMomyRequest $request, Momy $momy)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Momy  $momy
     * @return \Illuminate\Http\Response
     */
    public function destroy(Momy $momy)
    {
        //
    }
}
